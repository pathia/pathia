package cute.concolic.generateinputandschedule;

import cute.concolic.input.InputMap;
import cute.concolic.pathconstraint.PathConstraint;
import cute.concolic.symbolicexecution.ComputationStack;
import cute.concolic.symbolicstate.ArithmeticExpression;
import cute.concolic.symbolicstate.Expression;
import instrumented.java.util.Collections;
import cute.concolic.Globals;
import lpsolve.LpSolve;
import lpsolve.LpSolveException;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: Koushik Sen (ksen@cs.uiuc.edu)
 * Date: Jun 26, 2006
 * Time: 2:09:01 PM
 */
public class ArithmeticSolver {
    private PathConstraint path;
    private InputMap input;
    private int optimized;
    private int n;
    private int[] nodes;

    public void addEqual(int x, int y) {
        int k,i,j,tmp;

        i = nodes[x];
        j = nodes[y];
        if (i!=j) {
            if (j<i) {
                tmp = j;
                j=i;
                i = tmp;
            }
            for (k=1;k<=n;k++) {
                if (nodes[k]==j) {
                    nodes[k]=i;
                }
            }
        }
    }

    public boolean isEqual(int x, int y) {
        int i,j;
        i = nodes[x];
        j = nodes[y];
        return(i==j);
    }

    public void initDependency(int n){
        int i;

        this.n = n;
        nodes = new int[n+1];
        for (i=0;i<=n;i++) {
            nodes[i]=i;
        }
    }


    public void print(){
        int i;
        System.out.println("\n------------");
        for (i=0;i<=n;i++) {
            System.out.print(nodes[i]);
            System.out.println(" ");
        }
        System.out.println("\n------------");
    }

    public ArithmeticSolver(PathConstraint path, InputMap input, int optimized) {
        this.path = path;
        this.input = input;
        this.optimized = optimized;
    }

    private Vector getArithContsraints(int k){
        int i,j,x;
        boolean flag;
        Vector ret;
        ArithmeticExpression tmp;
        ArithmeticExpression tmp2;

        ret = new Vector(k);
        tmp = path.getArith(k);
        x = tmp.svar[0];
        ret.add(tmp);

        for (i=0;i<=k;i++) {
            tmp = path.getArith(i);
            if (tmp!=null) {
                if ((optimized & 1) == 1) {
                    x = tmp.svar[0];
                    j=1;
                } else {
                    j = 0;
                }
                for (;j<tmp.svar.length;j++) {
                    addEqual(x,tmp.svar[j]);
                }
            }
        }

        for (i=k-1;i>=0;i--) {
            tmp = path.getArith(i);
            if (tmp!=null) {
                flag = false;
                for (j=0;j<tmp.svar.length && !flag;j++) {
                    if (isEqual(x,tmp.svar[j]))
                        flag = true;
                }
                if ((optimized & 2)==2) {
                    for (j=k;j>i && flag;j--) {
                        tmp2 = path.getArith(j);
                        if (tmp2!=null && tmp.equals(tmp2)) {
                            flag = false;
                        }
                    }
                }
                if (flag) {
                    ret.add(tmp);
                }
            }
        }
        
        return ret;
    }

    /* Pathia code start */
    //this function's logic was completely altered from jcute for pathia.
    //therefore the following code is mixed - original jcute code and pathia code
    public boolean solveArith(int k,int counter){
        double inf;
        int i,status,sz;
        ArithmeticExpression tmp;
        ArithmeticExpression tmp2;
        LpSolve  lp;
        Vector lc;

        if (k>=0) {
            tmp2 = path.getArith(k);
            tmp2.invert();
            if ((optimized & 4)==4) {
                for (i=0;i<k;i++) {
                    tmp = path.getArith(i);
                    if (tmp!=null && tmp2.unsat(tmp)) {
                        return false;
                    }
                }
            }
        }

        int nvars = input.nSymbolicArithInputValues();
        initDependency(nvars);
        lc = getArithContsraints(k);
        
        //add all past inputs as '!=' constraints
        SortedArrayList notEqualConstraints = new SortedArrayList();
        for (Object val : Globals.pathiaData.allInputs) {
        	notEqualConstraints.add((int)val);
        }
        
        //also add '!=' if they appear in this particular path
        sz = lc.size();
        for (i=0;i<sz;i++) {
        	tmp = (ArithmeticExpression)lc.get(i);
        	if (tmp.type != ArithmeticExpression.NE)
        		continue;
            int val = (int)(tmp.coeff[0])*(int)(tmp.constant);
            notEqualConstraints.insertSorted(val);
        }
        
        //and finally add '!=' if it was given manualy by the user
        if(Globals.pathiaData.constraintGiven &&
        		Globals.pathiaData.opID == ArithmeticExpression.NE)
        	notEqualConstraints.insertSorted(Globals.pathiaData.constant);
        
        //now split k '!=' constraints into k+1 intervals
        boolean first = true;//first iteration
        int prevVal = 0, curVal = 0;//ends of the interval
        for (Object val : notEqualConstraints) {
        	if(first == true){
        		prevVal = (int)val;
        		first = false;
        		continue;
        	}
        	
        	curVal = (int)val;
        	
            try {
                lp = LpSolve.makeLp(0,nvars+1);
                
                inf = lp.getInfinite();
                for (i=1;i<=nvars+1;i++) {
                    if (i!=nvars+1) {
                        lp.setBounds(i,-inf,inf);
                        int typ = input.symbolicArithInputType(i);
                        
                        if(typ!= Globals.FLOAT && typ!=Globals.DOUBLE)
                            lp.setInt(i,true);
                    } else {
                        lp.setInt(i,true);
                    }
                }
                
                lp.setAddRowmode(true);
                tmp = new ArithmeticExpression(nvars+1);
                lp.setObjFnex(1,tmp.coeff,tmp.svar);

                sz = lc.size();
                for (i=0;i<sz;i++) {
                    tmp = (ArithmeticExpression)lc.get(i);
                    String str = new String();
                    if (tmp.type==ArithmeticExpression.NE) {
                        //already handled all '!=' constraints
                    } else if (tmp.type==ArithmeticExpression.EQ) {
                        lp.addConstraintex(tmp.svar.length,tmp.coeff,tmp.svar,LpSolve.EQ,tmp.constant);
                    } else {
                        lp.addConstraintex(tmp.svar.length,tmp.coeff,tmp.svar,tmp.type,tmp.constant);
                    }
                }
                
                //add constraint given manually by user
                if(Globals.pathiaData.constraintGiven &&
                		Globals.pathiaData.opID != ArithmeticExpression.NE){//case of
                		//Globals.pathiaData.opID == ArithmeticExpression.NE already handled above
                	int[] svar = new int[1];
                	svar[0] = 1;
                	double[] coeff = new double[1];
                	coeff[0] = 1.0;
                	if(Globals.pathiaData.opID == ArithmeticExpression.EQ)
                		lp.addConstraintex(svar.length, coeff, svar, LpSolve.EQ,
                				Globals.pathiaData.constant);
                	else
                		lp.addConstraintex(svar.length, coeff, svar, Globals.pathiaData.opID,
                				Globals.pathiaData.constant);
                }
                		
                lp.setAddRowmode(false);
                lp.setVerbose(LpSolve.CRITICAL);
                
                
                if(prevVal != Integer.MIN_VALUE && curVal - prevVal <= 1){
                	lp.deleteLp();
                	prevVal = curVal;
                	continue;
                }

                //limit LP solver to these bound
        		lp.setBounds(1,prevVal+1,curVal-1);
                status = lp.solve();
                    
                if (LpSolve.OPTIMAL == status) {//found solution
                	double[] sol;
                	sol = lp.getPtrPrimalSolution();
                	tmp = (ArithmeticExpression)lc.get(0);
                	if (counter==0)
                		inputArithUpdate(sol,lp.getNrows(),tmp.svar[0]);
                	lp.deleteLp();
                	return true;
                }
                else //didn't find solution in these interval, continue to next interval
                	lp.deleteLp();
                    
                    prevVal = curVal;
        		
            } catch (LpSolveException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }

        return false;
    }
    /* Pathia code end */

    private void inputArithUpdate(double[] sol, int begin, int x) {
        int j;
        int sz = input.nSymbolicArithInputValues();
        for(j=1;j<=sz;j++){
            if(isEqual(x,j))
                input.updateArithInput(j,sol[begin+j]);
        }
    }
}
