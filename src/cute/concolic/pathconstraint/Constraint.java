package cute.concolic.pathconstraint;

import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * Author: Koushik Sen <ksen@cs.uiuc.edu>
 */
public interface Constraint {
    public void printConstraint(PrintWriter out);
    
    /* Pathia code start */
    
    public String printConstraintToString(String str);
    
    /* Pathia code end */
}
