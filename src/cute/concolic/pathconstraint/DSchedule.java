package cute.concolic.pathconstraint;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Serializable;

/**
 *  .
 * User: ksen
 * Date: Oct 9, 2005
 * Time: 6:16:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class DSchedule implements Serializable, Constraint {
    /**
     *
     */
    private static final long serialVersionUID = 8965352739040973220L;
    public int pid;
    public int mid;
    public int nextPid;
    public int nextMid;
    public boolean isRace;

    public void printConstraint(PrintWriter out) {
        out.print("pid = " + pid);
        out.print("\tmid = " + mid);
        out.print("\tnextPid = " + nextPid);
        out.print("\tnextMid = " + nextMid);
        out.println("\tisRace = " + isRace);
    }
    
    /* Pathia code start */
    //similar to the above function (which was written for jcute)
    public String printConstraintToString(String str) {
    	str = str + "pid = " + pid;
    	str = str + "\tmid = " + mid;
    	str = str + "\tnextPid = " + nextPid;
    	str = str + "\tnextMid = " + nextMid;
    	str = str + "\tisRace = " + isRace;
    	return str;
    }
    
    /* Pathia code end */
}
