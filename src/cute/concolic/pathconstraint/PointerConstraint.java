package cute.concolic.pathconstraint;

import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * Author: Koushik Sen <ksen@cs.uiuc.edu>
 */
public class PointerConstraint implements Constraint{
    public int first;
    public int second;
    public boolean equal;

    public PointerConstraint(int first, int second, boolean equal) {
        this.first = first;
        this.second = second;
        this.equal = equal;
    }

    public void printConstraint(PrintWriter out) {
        if(first==0){
            out.print("null");
        } else {
            out.print("p");
            out.print(first);
        }
        if(equal){
            out.print(" == ");
        } else {
            out.print(" != ");
        }
        if(second==0){
            out.println("null");
        } else {
            out.print("p");
            out.println(second);
        }
    }
    
    /* Pathia code start */
    //similar to the above function (which was written for jcute)
    public String printConstraintToString(String str) {
        if(first==0){
        	str = str + "null";
        } else {
        	str = str + "p";
        	str = str + first;
        }
        if(equal){
        	str = str + " == ";
        } else {
        	str = str + " != ";
        }
        if(second==0){
        	str = str + "null";
        } else {
        	str = str + "p";
        	str = str + second;
        }
        return str;
    }
    
    /* Pathia code end */
}
