package cute.concolic.input;

import java.io.File;
import java.util.Scanner;

import cute.Input;
import cute.concolic.Globals;
import cute.concolic.symbolicexecution.ComputationStack;

/**
 * Created by IntelliJ IDEA.
 * User: Koushik Sen (ksen@cs.uiuc.edu)
 * Date: Dec 16, 2005
 * Time: 9:19:09 AM
 */
public class InputImpl implements Input {

    public Object Object(String className){
        if(!Globals.globals.initialized){
            Globals.globals.begin();
        }
        return Globals.globals.input.ObjectAux(className,true);
    }

    public  Object ObjectShallow(String className){
        if(!Globals.globals.initialized){
            Globals.globals.begin();
        }
        return Globals.globals.input.ObjectAux(className,false);
    }

    public int Integer(){
        if(!Globals.globals.initialized){
            Globals.globals.begin();
        }
        
        /* Pathia code start */
        
        //if simulation mode, simply use the input that needs to be simulated
        if(Globals.globals.information.simulationMode){
        	return Globals.pathiaData.intUsedForSim;
        }
        
        //get the ready input, and print it
        int inp = ((Integer)Globals.globals.input.myInput(Globals.INT, null)).intValue();
        
        if(Globals.globals.information.firstFunc)
        	System.out.print("Input: "+inp+" (generated for function 1)\nPath in function 1: ");
        else
        	System.out.print("Input: "+inp+" (generated for function 2)\nPath in function 2: ");
        
        //add input to the vector of all printed inputs
        Globals.pathiaData.allInputs.insertSorted(inp);
        
        //update maxInput field
        if(inp > Globals.pathiaData.maxInput){
        	Globals.pathiaData.maxInput = inp;
        }
        
        //update minInput field
        if(inp < Globals.pathiaData.maxInput){
        	Globals.pathiaData.minInput = inp;
        }

        return inp;
        
        /* Pathia code end */
    }

    public  short Short(){
        if(!Globals.globals.initialized){
            Globals.globals.begin();
        }
        return ((Short)Globals.globals.input.myInput(Globals.SHORT, null)).shortValue();
    }

    public  long Long(){
        if(!Globals.globals.initialized){
            Globals.globals.begin();
        }
        return ((Long)Globals.globals.input.myInput(Globals.LONG, null)).longValue();
    }

    public  byte Byte(){
        if(!Globals.globals.initialized){
            Globals.globals.begin();
        }
        return ((Byte)Globals.globals.input.myInput(Globals.BYTE, null)).byteValue();
    }

    public  char Character(){
        if(!Globals.globals.initialized){
            Globals.globals.begin();
        }
        return ((Character)Globals.globals.input.myInput(Globals.CHAR, null)).charValue();
    }

    public  float Float(){
        if(!Globals.globals.initialized){
            Globals.globals.begin();
        }
        return ((Float)Globals.globals.input.myInput(Globals.FLOAT, null)).floatValue();
    }

    public  double Double(){
        if(!Globals.globals.initialized){
            Globals.globals.begin();
        }
        return ((Double)Globals.globals.input.myInput(Globals.DOUBLE, null)).doubleValue();
    }

    public  boolean Boolean(){
        if(!Globals.globals.initialized){
            Globals.globals.begin();
        }
        return ((Boolean)Globals.globals.input.myInput(Globals.BOOLEAN, null)).booleanValue();
    }

}
