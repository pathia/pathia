package cute;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.TreeMap;

import org.apache.commons.io.FileUtils;

import ccl.util.FileUtil;
import cute.concolic.Globals;
import cute.concolic.generateinputandschedule.SortedArrayList;
import cute.concolic.input.InputImpl;
import cute.concolic.logging.BranchCoverageLog;
import cute.concolic.symbolicexecution.ComputationStack;
import cute.concolic.symbolicstate.ArithmeticExpression;
import cute.gui.CommandLine;
import cute.gui.JCuteGui;
import cute.gui.JCuteTextUI;
import junit.framework.TestCase;

/**
 * Author: Koushik Sen <ksen@cs.uiuc.edu>
 */

//@todo array inputs
//@todo handling unknown (library) functions with concurrency primitive

public class Cute {
    public static int N;
    public static final int EXIT_COMPLETE = 2;
    public static final int EXIT_ASSERT_FAILED = 32;
    public static final int EXIT_ERROR = 8;
    public static final int EXIT_RACE = 4;
    public static final int EXIT_DEADLOCK = 16;

    public static Input input = new InputImpl();

    public final static String pathSeparator=System.getProperty("path.separator");
    public final static String fileSeparator = System.getProperty("file.separator");
    
    public static void main(String[] args) {
    	/* Pathia code start */

    	//temporary folders for this execution
        File tmpDirFunc1 = new File("tmp_pathia_func1");
        File tmpDirFunc1Sim = new File("tmp_pathia_func1_sim");
        File classDirFunc1 = new File("tmp_pathia_func1\\classes");
        File classDirFunc1Sim = new File("tmp_pathia_func1_sim\\classes");
        
        File tmpDirFunc2 = new File("tmp_pathia_func2");
        File tmpDirFunc2Sim = new File("tmp_pathia_func2_sim");
        File classDirFunc2 = new File("tmp_pathia_func2\\classes");
        File classDirFunc2Sim = new File("tmp_pathia_func2_sim\\classes");
        
        //shared data holders between this program to the jcute program
        PathiaData dataFunc1 = new PathiaData("tmp_pathia_func1\\");
        PathiaData dataFunc2 = new PathiaData("tmp_pathia_func2\\");
        PathiaData dataFunc1Sim = new PathiaData("tmp_pathia_func1_sim\\");
        PathiaData dataFunc2Sim = new PathiaData("tmp_pathia_func2_sim\\");

        //remove all temporary files and directories from previous execution
        try{ 
        	FileUtils.deleteDirectory(tmpDirFunc1);
        	FileUtils.deleteDirectory(tmpDirFunc1Sim);
        	FileUtils.deleteDirectory(tmpDirFunc2);
        	FileUtils.deleteDirectory(tmpDirFunc2Sim);
        }
        catch (Exception e) {}
        
        //create the directories
    	if(!tmpDirFunc1.exists())
    		tmpDirFunc1.mkdir();
    	if(!tmpDirFunc1Sim.exists())
    		tmpDirFunc1Sim.mkdir();
    	if(!classDirFunc1.exists())
    		classDirFunc1.mkdir();
    	if(!classDirFunc1Sim.exists())
    		classDirFunc1.mkdir();
    	
    	if(!tmpDirFunc2.exists())
    		tmpDirFunc2.mkdir();
    	if(!tmpDirFunc2Sim.exists())
    		tmpDirFunc2Sim.mkdir();
    	if(!classDirFunc2.exists())
    		classDirFunc2.mkdir();
    	if(!classDirFunc2Sim.exists())
    		classDirFunc2.mkdir();
		
    	//more relevant paths
    	File funcDir = new File("function\\appended");//contains the source function of the user
    	File binDir = new File("bin");//binary classes (to compile together with the
    									//user source function)
    	File jlibsDir = new File("jlibs");//lpsolve libraries
    	File libDir = new File("lib");//more jar libraries
    	
    	//set all relevant environment variables
        String[] envp = new String[4];
        envp[0] = "LD_LIBRARY_PATH="+jlibsDir.getAbsolutePath();
        envp[1] = "JAVA_LIB_PATH="+jlibsDir.getAbsolutePath();
        envp[2] = "PATH="+System.getProperty("java.library.path")+
        		pathSeparator+jlibsDir.getAbsolutePath();
        envp[3] = "CLASSPATH="+classDirFunc1.getAbsolutePath()+
        		pathSeparator+classDirFunc1Sim.getAbsolutePath()+
        		pathSeparator+classDirFunc2.getAbsolutePath()+
        		pathSeparator+classDirFunc2Sim.getAbsolutePath()+
        		pathSeparator+binDir.getAbsolutePath()+
        		pathSeparator+System.getProperty("java.class.path");
        //add all jars in lib to classpath
        File[] jars = libDir.listFiles();
        for (int i = 0; i < jars.length; i++)
        	envp[3] = envp[3] + pathSeparator + jars[i].getAbsolutePath();
        
        //print enviroment variables
        /*for (int i = 0; i < envp.length; i++) {
            String s = envp[i];
            System.out.println("env["+i+"]:"+s);
        }*/
        
        //create java classes from the users source functions by wrapping the functions
        //with class definitions
		try {
			//the file parts to append
			File Class1Head = new File("function\\sys\\class1_head.txt");
			File Class1SimHead = new File("function\\sys\\class1_head_sim.txt");
			
			File Class2Head = new File("function\\sys\\class2_head.txt");
			File Class2SimHead = new File("function\\sys\\class2_head_sim.txt");
			
			File Func1Body = new File("function\\func1.txt");
			File Func2Body = new File("function\\func2.txt");
			
			File ClassBottom = new File("function\\sys\\class_bottom.txt");

			//the files that will contain the entire class definitions
			File Class1Entire = new File("function\\appended\\ConcolicFunc1.java");
			File Class1SimEntire = new File("function\\appended\\ConcolicFunc1Sim.java");
			File Class2Entire = new File("function\\appended\\ConcolicFunc2.java");
			File Class2SimEntire = new File("function\\appended\\ConcolicFunc2Sim.java");

			//finally, write to files
			
			//function 1:
			FileUtils.write(Class1Entire,
					FileUtils.readFileToString(Class1Head));
			FileUtils.write(Class1Entire,
					FileUtils.readFileToString(Func1Body), true); // true for append
			FileUtils.write(Class1Entire,
					FileUtils.readFileToString(ClassBottom), true); // true for append
			
			//function 1 for simulations:
			FileUtils.write(Class1SimEntire,
					FileUtils.readFileToString(Class1SimHead));
			FileUtils.write(Class1SimEntire,
					FileUtils.readFileToString(Func1Body), true); // true for append
			FileUtils.write(Class1SimEntire,
					FileUtils.readFileToString(ClassBottom), true); // true for append
			
			//function 2:
			FileUtils.write(Class2Entire,
					FileUtils.readFileToString(Class2Head));
			FileUtils.write(Class2Entire,
					FileUtils.readFileToString(Func2Body), true); // true for append
			FileUtils.write(Class2Entire,
					FileUtils.readFileToString(ClassBottom), true); // true for append
			
			//function 2 for simulations:
			FileUtils.write(Class2SimEntire,
					FileUtils.readFileToString(Class2SimHead));
			FileUtils.write(Class2SimEntire,
					FileUtils.readFileToString(Func2Body), true); // true for append
			FileUtils.write(Class2SimEntire,
					FileUtils.readFileToString(ClassBottom), true); // true for append
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
        //compilation commands for the source classes
        String compCommandFunc1 = "javac -d " + classDirFunc1.getAbsolutePath() +
        		" -sourcepath " + funcDir.getAbsolutePath() + " " +
        		funcDir.getAbsolutePath()+"\\ConcolicFunc1.java";
        String compCommandFunc1Sim = "javac -d " + classDirFunc1.getAbsolutePath() +
        		" -sourcepath " + funcDir.getAbsolutePath() + " " +
        		funcDir.getAbsolutePath()+"\\ConcolicFunc1Sim.java";
        
        String compCommandFunc2 = "javac -d " + classDirFunc2.getAbsolutePath() +
        		" -sourcepath " + funcDir.getAbsolutePath() + " " +
        		funcDir.getAbsolutePath()+"\\ConcolicFunc2.java";
        String compCommandFunc2Sim = "javac -d " + classDirFunc2.getAbsolutePath() +
        		" -sourcepath " + funcDir.getAbsolutePath() + " " +
        		funcDir.getAbsolutePath()+"\\ConcolicFunc2Sim.java";
        
        //execute the compilation for the 1st function
        System.out.print("Compiling function 1... ");
        int exitCode = CommandLine.executeOnce(compCommandFunc1,envp,tmpDirFunc1,null,null,true);
        if(exitCode != 0) {
            System.err.println("ERROR: FUNCTION 1 COMPILATION FAILED");
            return;
        }
        else
        	System.out.println("Function 1 compilation succeeded.");
        
        exitCode = CommandLine.executeOnce(compCommandFunc1Sim,envp,tmpDirFunc1Sim,null,null,true);
        
        //execute the compilation for the 2nd function
        System.out.print("Compiling function 2... ");
        exitCode = CommandLine.executeOnce(compCommandFunc2,envp,tmpDirFunc2,null,null,true);
        if(exitCode != 0) {
            System.err.println("ERROR: FUNCTION 2 COMPILATION FAILED");
            return;
        }
        else
        	System.out.println("Function 2 compilation succeeded.");
        
        exitCode = CommandLine.executeOnce(compCommandFunc2Sim,envp,tmpDirFunc2Sim,null,null,true);
        
        //the instrumentation commands on the compiled class files
        String instCommandFunc1 = "java -Xmx512m -Xms512m -Dcute.sequential=true " +
        		"cute.instrument.CuteInstrumenter -keep-line-number -d " +
        		classDirFunc1.getAbsolutePath() +
                " -x com.vladium -x cute -x lpsolve --app " + "ConcolicFunc1";
        String instCommandFunc1Sim = "java -Xmx512m -Xms512m -Dcute.sequential=true " +
        		"cute.instrument.CuteInstrumenter -keep-line-number -d " +
        		classDirFunc1.getAbsolutePath() +
                " -x com.vladium -x cute -x lpsolve --app " + "ConcolicFunc1Sim";
        
        String instCommandFunc2 = "java -Xmx512m -Xms512m -Dcute.sequential=true " +
        		"cute.instrument.CuteInstrumenter -keep-line-number -d " +
        		classDirFunc2.getAbsolutePath() +
                " -x com.vladium -x cute -x lpsolve --app " + "ConcolicFunc2";
        String instCommandFunc2Sim = "java -Xmx512m -Xms512m -Dcute.sequential=true " +
        		"cute.instrument.CuteInstrumenter -keep-line-number -d " +
        		classDirFunc2.getAbsolutePath() +
                " -x com.vladium -x cute -x lpsolve --app " + "ConcolicFunc2Sim";
        
        //execute the instrumentation for the 1st function
        System.out.print("Instrumenting function 1... ");
        exitCode = CommandLine.executeOnce(instCommandFunc1,envp,tmpDirFunc1,null,null,false);
        if(exitCode != 0) {
        	System.err.println("ERROR: FUNCTION 1 INSTRUMENTATION FAILED");
            return;
        }
        else
        	System.out.println("Function 1 instrumentation succeeded.");
        
        exitCode = CommandLine.executeOnce(instCommandFunc1Sim,envp,tmpDirFunc1Sim,null,null,false);

        //execute the instrumentation for the 2nd function
        System.out.print("Instrumenting function 2... ");
        exitCode = CommandLine.executeOnce(instCommandFunc2,envp,tmpDirFunc2,null,null,false);
        if(exitCode != 0) {
        	System.err.println("ERROR: FUNCTION 2 INSTRUMENTATION FAILED");
            return;
        }
        else
        	System.out.println("Function 2 instrumentation succeeded.");
        
        exitCode = CommandLine.executeOnce(instCommandFunc2Sim,envp,tmpDirFunc2Sim,null,null,false);
        
        //commands for the execution of the concolic run on the instrumented class files
        String concCommandFunc1 = "java -ea -Xmx512m -Xms512m -Djava.library.path=" +
        		jlibsDir.getAbsolutePath() +
        		" -Dcute.args=-m:0:-d:0:-p:1:-v:-j:-w: cute.RunOnce ConcolicFunc1.main";
        String concCommandFunc1Sim = "java -ea -Xmx512m -Xms512m -Djava.library.path=" +
        		jlibsDir.getAbsolutePath() +
        		" -Dcute.args=-m:0:-d:0:-p:1:-v:-j:-z: cute.RunOnce ConcolicFunc1Sim.main";
        
        
        String concCommandFunc2 = "java -ea -Xmx512m -Xms512m -Djava.library.path=" +
        		jlibsDir.getAbsolutePath() +
        		" -Dcute.args=-m:0:-d:0:-p:1:-v:-j: cute.RunOnce ConcolicFunc2.main";
        String concCommandFunc2Sim = "java -ea -Xmx512m -Xms512m -Djava.library.path=" +
        		jlibsDir.getAbsolutePath() +
        		" -Dcute.args=-m:0:-d:0:-p:1:-v:-j:-z: cute.RunOnce ConcolicFunc2Sim.main";
        
        //first complete an entire loop of concolic run on all of the paths
        //to discover all branches and output them
        System.out.println("Loading branch tables... \n");
        System.out.println("*************************************************************\n");
        
        //function 1 loop:
        while(true) {//each iteration of this loop covers a single path
        	exitCode = CommandLine.executeOnce(concCommandFunc1,envp,tmpDirFunc1,null,null,false);
        	if(exitCode != 0 && exitCode != 2){//concolic run failed
        		System.err.println("ERROR: FUNCTION 1 CONCOLIC RUN FAILED");
        		return;
        	}

        	if(exitCode == 2)
        		//then execution ended and all branches that the engine
        		//can find are found
        		break;
        }
        
        //function 2 loop:
        while(true) {//each iteration of this loop covers a single path
        	exitCode = CommandLine.executeOnce(concCommandFunc2,envp,tmpDirFunc2,null,null,false);
        	if(exitCode != 0 && exitCode != 2){//concolic run failed
        		System.err.println("ERROR: FUNCTION 2 CONCOLIC RUN FAILED");
        		return;
        	}
        	
        	if(exitCode == 2)
        		//then execution ended and all branches that the engine
        		//can find are found
        		break;
        }

        //load the data from both executions and print branch tables from them
        dataFunc1.readData();
        dataFunc2.readData();
        
        //function 1 branch table:
        System.out.println("BRANCH TABLE FOR FUNCTION 1:");
        for(int i = 0; i <= dataFunc1.maxBranchId; i++ ){
        	String str = (String) dataFunc1.branchIds.get(i);
        	if(str != null)
        		System.out.println(str);
        }
        System.out.println();
        
        //function 1 branch table:
        System.out.println("BRANCH TABLE FOR FUNCTION 2:");
        for(int i = 0; i <= dataFunc2.maxBranchId; i++ ){
        	String str = (String) dataFunc2.branchIds.get(i);
        	if(str != null)
        		System.out.println(str);
        	
        }
        System.out.println();
        System.out.println("*************************************************************\n");
        
        System.out.println("In the following concolic run the paths will be printed as a list of branch "
        		+ "ids followed by 1 for a taken branch and 0 for a not taken branch.\n");
        
        //clear directories towards the actual concolic run
        (new File("tmp_pathia_func1\\pathiaData")).delete();
        (new File("tmp_pathia_func1_sim\\pathiaData")).delete();
        (new File("tmp_pathia_func2\\pathiaData")).delete();
        (new File("tmp_pathia_func2_sim\\pathiaData")).delete();

        
        //perform the actual concolic run. the following outer loop asks the user
        //each time for how many more iterations does he wish to activate the
        //concolic tester
        
        boolean first = true;//first iteration 
        
 outerloop:
	 
	 	while(true){
        	if(!first){//if not first iteration ask the user if he wishes to continue
        				//generating inputs on top of what was already generated
        		while(true){//loop until valid answer from user
        			System.out.println("Would you like to continue generating inputs? (Y\\N)");
        			
        			Scanner scanner = new Scanner(System.in);
        			String inpStr = scanner.nextLine();
        			System.out.println();
        			
        			if(inpStr.equalsIgnoreCase("N"))
        				break outerloop;//finish execution
        			else if (inpStr.equalsIgnoreCase("Y"))
        				break;
        			else
        				System.out.println("Invalid input: please enter 'Y' or 'N'\n");
        		}
        		
        	}
        	first = false;
        	
        	int numIter = 0;
        	//ask user for how many iterations to run
    		while(true){//loop until valid answer from user
    			System.out.println("For how many iterations do you want the concolic tester to run? (An iteration is a single coverage of all paths that weren't exhausted yet)");
    			
    			Scanner scanner = new Scanner(System.in);
    			String inpStr = scanner.nextLine();
    			System.out.println();
    			
    			if(!inpStr.matches("^-?\\d+$")){
    				System.out.println("Invalid input: please enter a number");
    				System.out.println();
    			}
    			else {
    				numIter = Integer.parseInt(inpStr);
    				break;
    			}
    		}
	    	
	    	//clear manual constraints for the new iteration
	    	dataFunc1.readData();
	    	dataFunc1.constraintGiven = false;
	    	dataFunc1.writeData();
	    	
	    	dataFunc2.readData();
	    	dataFunc2.constraintGiven = false;
	    	dataFunc2.writeData();
	    	
	    	//ask user if he wishes to enter a manual constraint e.g. 'x <= 2'
	    	boolean manualConstraint = false;
    		while(true){//loop until valid answer from user
    			System.out.println("Would you like to enter a manual constraint for the "
    					+ "following iteration? (Y\\N)");
    			
    			Scanner scanner = new Scanner(System.in);
    			String inpStr = scanner.next();
    			System.out.println();
    			
    			if(inpStr.equalsIgnoreCase("N"))
    				break;
    			else if (inpStr.equalsIgnoreCase("Y")){
    				manualConstraint = true;
    				break;
    			}
    			else
    				System.out.println("Invalid input: please enter 'Y' or 'N'\n");
    		}
    		
    		if(manualConstraint){
    			dataFunc1.readData();
    			dataFunc2.readData();
    			dataFunc1.constraintGiven = true;
    			dataFunc2.constraintGiven = true;
    			//ask user to type his constraint
	    		while(true){//loop until valid answer from user
	    			System.out.println("Please enter you constraint in the form: "
	    					+ "'x op constant'. E.g.: x <= 2");
	    			
	    	    	Scanner scanner = new Scanner(System.in);
	    			String constrStr = scanner.nextLine();
	    			String[] splitConstr = constrStr.split("\\s+");
	    			System.out.println();
	    			
	    			if(splitConstr.length != 3 || !splitConstr[0].equalsIgnoreCase("x") || 
	    					!splitConstr[2].matches("^-?\\d+$")/*checks if integer*/ || 
	    					(!splitConstr[1].equals("<=") && !splitConstr[1].equals("=<") &&
	    					!splitConstr[1].equals(">=") && !splitConstr[1].equals("=>") &&
	    					!splitConstr[1].equals("<") && !splitConstr[1].equals(">") &&
	    					!splitConstr[1].equals("==") && !splitConstr[1].equals("=") &&
	    					!splitConstr[1].equals("!=")))
	    				System.out.println("Invalid input\n");
	    			else{
	    				//find out which operator the user typed and set the constraint
	    				//in the data holder for the following execution
	    				if(splitConstr[1].equals("<=") || splitConstr[1].equals("=<")){
		    		    	dataFunc1.opID = ArithmeticExpression.LE;
		    		    	dataFunc1.constant = Integer.parseInt(splitConstr[2]);
		    		    	dataFunc2.opID = ArithmeticExpression.LE;
		    		    	dataFunc2.constant = Integer.parseInt(splitConstr[2]);
		    				break;
	    				}
	    				else if(splitConstr[1].equals(">=") || splitConstr[1].equals("=>")){
		    		    	dataFunc1.opID = ArithmeticExpression.GE;
		    		    	dataFunc1.constant = Integer.parseInt(splitConstr[2]);
		    		    	dataFunc2.opID = ArithmeticExpression.GE;
		    		    	dataFunc2.constant = Integer.parseInt(splitConstr[2]);
		    				break;
	    				}
	    				else if(splitConstr[1].equals("<")){
		    		    	dataFunc1.opID = ArithmeticExpression.LE;
		    		    	dataFunc1.constant = Integer.parseInt(splitConstr[2]) - 1;
		    		    	dataFunc2.opID = ArithmeticExpression.LE;
		    		    	dataFunc2.constant = Integer.parseInt(splitConstr[2]) - 1;
		    				break;
	    				}
	    				else if(splitConstr[1].equals(">")){
		    		    	dataFunc1.opID = ArithmeticExpression.GE;
		    		    	dataFunc1.constant = Integer.parseInt(splitConstr[2]) + 1;
		    		    	dataFunc2.opID = ArithmeticExpression.GE;
		    		    	dataFunc2.constant = Integer.parseInt(splitConstr[2]) + 1;
		    				break;
	    				}
	    				else if(splitConstr[1].equals("==") || splitConstr[1].equals("=")){
	    					//in equality constraint return error if the constant already printed
	    					//as input
	    					if(dataFunc1.allInputs.contains(Integer.parseInt(splitConstr[2])) ||
	    							dataFunc2.allInputs.contains(Integer.parseInt(splitConstr[2]))){
	    						System.out.println("Error: You entered an equality with a constant "
	    								+ "that was already printed as input. Try again.\n");
	    						continue;
	    					}
		    		    	dataFunc1.opID = ArithmeticExpression.EQ;
		    		    	dataFunc1.constant = Integer.parseInt(splitConstr[2]);
		    		    	dataFunc2.opID = ArithmeticExpression.EQ;
		    		    	dataFunc2.constant = Integer.parseInt(splitConstr[2]);
		    				break;
	    				}
	    				else if(splitConstr[1].equals("!=")){
		    		    	dataFunc1.opID = ArithmeticExpression.NE;
		    		    	dataFunc1.constant = Integer.parseInt(splitConstr[2]);
		    		    	dataFunc2.opID = ArithmeticExpression.NE;
		    		    	dataFunc2.constant = Integer.parseInt(splitConstr[2]);
		    				break;
	    				}
	    			}
	    		}
    			dataFunc1.writeData();
    			dataFunc2.writeData();
    		}
	    	
	    	
	        
	        System.out.println("Executing concolic run for "+numIter+" iterations...\n");
	        System.out.println("*************************************************************\n");
	        
	        for(int i = 0; i < numIter; i++) {//each iteration covers once all of the paths
	        								//that hadn't been exhausted
	        	
	        	//function 1 concolic run:
	        	while(true){//each iteration of this loop covers a single path
		        	exitCode = CommandLine.executeOnce(concCommandFunc1,envp,tmpDirFunc1,null,null,true);
		        	
		        	//print path of the above execution
		        	System.out.print("Path in function 2: ");
		        	
		        	//read the input from the saved data files
		        	File inputLog = new File("tmp_pathia_func1\\cuteInputLog");
		        	int inp = -999;
	        		try{
	        			Scanner scanner = new Scanner(inputLog);
	        			scanner.useDelimiter("[^0-9-]+");
	        			inp = scanner.nextInt();
	        		
	        			scanner.close();
	        		}
	        		catch (Exception e) {}
	        		
	        		//add the input to the inputs container
	        		dataFunc2.readData();
        			if(!dataFunc2.allInputs.contains(inp)){
        				dataFunc2.allInputs.insertSorted(inp);
        				if(inp > dataFunc2.maxInput)
        					dataFunc2.maxInput = inp;
        			}
        			dataFunc2.writeData();
        			
        			//add the input as a field for the following simulation of function 2
        			//on this input to discover its path on this input
	        		dataFunc2Sim.readData();
        			dataFunc2Sim.intUsedForSim = inp;
        			dataFunc2Sim.writeData();
		        	
        			//the simulation
		        	CommandLine.executeOnce(concCommandFunc2Sim,envp,tmpDirFunc2Sim,null,null,true);
		        	System.out.println();
		        	
		        	if(exitCode != 0 && exitCode != 2){//concolic run failed
		        		System.err.println("ERROR: FUNCTION 1 CONCOLIC RUN FAILED");
		        		return;
		        	}
	
		        	if(exitCode == 2)//all unexhausted paths visited once
		        		break;
	        	}
	        	System.out.println("-------------------------------\n");
	        	
	        	//function 2 concolic run:
	        	while(true){//each iteration of this loop covers a single path
		        	exitCode = CommandLine.executeOnce(concCommandFunc2,envp,tmpDirFunc2,null,null,true);
		        	
		        	//print path of the above execution
		        	System.out.print("Path in function 1: ");
	        		
		        	File inputLog = new File("tmp_pathia_func2\\cuteInputLog");
		        	int inp = -999;
	        		try{
	        			Scanner scanner = new Scanner(inputLog);
	        			scanner.useDelimiter("[^0-9-]+");
	        			inp = scanner.nextInt();
	        		
	        			scanner.close();
	        		}
	        		catch (Exception e) {}
	        		
	        		//add the input to the inputs container
	        		dataFunc1.readData();
        			if(!dataFunc1.allInputs.contains(inp)){
        				dataFunc1.allInputs.insertSorted(inp);
        				if(inp > dataFunc1.maxInput)
        					dataFunc1.maxInput = inp;
        			}
        			dataFunc1.writeData();
        			
        			//add the input as a field for the following simulation of function 1
        			//on this input to discover its path on this input
	        		dataFunc1Sim.readData();
        			dataFunc1Sim.intUsedForSim = inp;
        			dataFunc1Sim.writeData();

        			//the simulation
		        	CommandLine.executeOnce(concCommandFunc1Sim,envp,tmpDirFunc1Sim,null,null,true);
		        	System.out.println();
		        	
		        	if(exitCode != 0 && exitCode != 2){//concolic run failed
		        		System.err.println("ERROR: FUNCTION 2 CONCOLIC RUN FAILED");
		        		return;
		        	}
	
		        	if(exitCode == 2)//all unexhausted paths visited once
		        		break;
	        	}
	        	
	        	if(i < numIter-1)
	        		System.out.println("###############################\n");
	        	
	        }
	    
	        System.out.println("*************************************************************\n");
	        
	        //print path statistics:
	        
	        //read data from file
	        dataFunc1.readData();
	        dataFunc1Sim.readData();
	        dataFunc2.readData();
	        dataFunc2Sim.readData();
	        
	        //find maximal visit count from all paths for indication on which
	        //paths had been exhausted (function 1)
	        System.out.println("PATHS STATISTICS FOR FUNCTION 1:");
	        int maxCount = 0;
	        for(Object path: dataFunc1.allPathsNoManConstr.keySet()){
	        	String pathString = (String)path;
	        	int count = (int) dataFunc1.allPathsNoManConstr.get(pathString);
	        	if(count > maxCount)
	        		maxCount = count;
	        }
	        
	        //print path statistics (function 1)
	        for(Object path: dataFunc1.allPaths.keySet()){
	        	String pathString = (String)path;
	        	int count = (int) dataFunc1.allPaths.get(pathString);
	        	Integer countSim = (Integer) dataFunc1Sim.allPaths.get(path);
	        	int total = count;
	        	
	        	int countNoConstr = (int) dataFunc1.allPathsNoManConstr.get(pathString);
	        	
	        	if(countSim != null)
	        		total += countSim.intValue();
	        	
	        	System.out.print("Path: "+pathString+" | Count: "+total+" | Status: ");
	        	if(countNoConstr < maxCount)
	        		System.out.println("Exhausted");
	        	else
	        		System.out.println("Active");
	        }
	        
	        //print paths that only discovered in simulations (function 1)
	        for(Object path: dataFunc1Sim.allPaths.keySet()){
	        	String pathString = (String)path;
	        	if(dataFunc1.allPaths.get(pathString) != null)//already printed this path
	        		continue;
	        	int count = (int) dataFunc1Sim.allPaths.get(pathString);
	        	
	        	System.out.println("Path: "+pathString+" | Count: "+count+" | Status: Exhausted");
	        }
	        
	        //find maximal visit count from all paths for indication on which
	        //paths had been exhausted (function 2)
	        System.out.println("\nPATHS STATISTICS FOR FUNCTION 2:");
	        maxCount = 0;
	        for(Object path: dataFunc2.allPaths.keySet()){
	        	String pathString = (String)path;
	        	int count = (int) dataFunc2.allPathsNoManConstr.get(pathString);
	        	if(count > maxCount)
	        		maxCount = count;
	        }
	        
	        //print path statistics (function 2)
	        for(Object path: dataFunc2.allPaths.keySet()){
	        	String pathString = (String)path;
	        	int count = (int) dataFunc2.allPaths.get(pathString);
	        	Integer countSim = (Integer) dataFunc2Sim.allPaths.get(path);
	        	int total = count;
	        	
	        	int countNoConstr = (int) dataFunc2.allPaths.get(pathString);
	        	
	        	if(countSim != null)
	        		total += countSim.intValue();
	        	
	        	System.out.print("Path: "+pathString+" | Count: "+total+" | Status: ");
	        	if(countNoConstr < maxCount)
	        		System.out.println("Exhausted");
	        	else
	        		System.out.println("Active");
	        }
	        
	        //print paths that only discovered in simulations (function 2)
	        for(Object path: dataFunc2Sim.allPaths.keySet()){
	        	String pathString = (String)path;
	        	if(dataFunc2.allPaths.get(pathString) != null)//already printed this path
	        		continue;
	        	int count = (int) dataFunc2Sim.allPaths.get(pathString);
	        	
	        	System.out.println("Path: "+pathString+" | Count: "+count+" | Status: Exhausted");
	        }
	        
	        System.out.println("\n*************************************************************\n");
	        
        }
        
        //remove all temporary files and directories
        try{ 
        	FileUtils.deleteDirectory(tmpDirFunc1);
        	FileUtils.deleteDirectory(tmpDirFunc1Sim);
        	FileUtils.deleteDirectory(tmpDirFunc2);
        	FileUtils.deleteDirectory(tmpDirFunc2Sim);
        }
        catch (Exception e) {}
        
        System.out.println("Execution done.");
        
        /* Pathia code end */
        
    }

    public static void Assume(boolean b){
        if(!b) {
            if(Globals.globals.initialized){
                if(Globals.globals.information.brackTrackAt<0)
                    Globals.globals.information.brackTrackAt = Globals.globals.path.size()-1;
                Thread.currentThread().stop();
            } else {
                System.exit(0);
            }
        }
    }

    public static void Assert(boolean b){
        if(!b) {
            if(Globals.globals.initialized){
                System.err.println("Assertion failed");
                Thread.dumpStack();
                System.err.println("Exiting with error");
                Globals.globals.information.returnVal = Cute.EXIT_ASSERT_FAILED+Globals.globals.information.returnVal;
                Globals.globals.solver.predict();
            } else {
                TestCase.assertTrue(b);
            }
        }
    }


}
