package cute.gui;

import java.io.*;

/**
 *  .
 * User: Koushik Sen (ksen@cs.uiuc.edu)
 * Date: Oct 29, 2005
 * Time: 9:10:13 PM
 */
public class CommandLine extends Thread {
    private static boolean optionPrintOutput;
    private InputStream is;
    private OutputLogger output;
    private String style;

    /* Pathia code start */
    //some lines modified to block printing and change the printing medium (console instead of PrintWriter).
    public static int executeOnce(String s,String[] envp,File workingDir,
                                  OutputLogger output,ProcessOwner po,boolean optionPrintOutputA){
        optionPrintOutput = optionPrintOutputA;
        try {
            Runtime rt = Runtime.getRuntime();
          //  if(optionPrintOutput) System.out.println("cd "+workingDir.getAbsolutePath());
           // if(optionPrintOutput) System.out.println(s);
            Process proc = rt.exec(s,envp,workingDir);
           // po.setProcess(proc);
            CommandLine errorProcessor = new
                    CommandLine(proc.getErrorStream(),output,"error");
            CommandLine outputProcessor = new
                    CommandLine(proc.getInputStream(),output,"regular");

            errorProcessor.start();
            outputProcessor.start();
            PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(proc.getOutputStream())));
            out.print(s);
            out.close();
            proc.waitFor();
           // po.setProcess(null);
            outputProcessor.join();
            errorProcessor.join();
          //  if(optionPrintOutput) System.out.println("Exit "+proc.exitValue()+"\n");
            return proc.exitValue();
        } catch(IOException ioe){
            ioe.printStackTrace();
            return 1;
        } catch(InterruptedException ie){
            ie.printStackTrace();
            return 1;
        }
    }
    /* Pathia code end */


    public CommandLine(InputStream is,OutputLogger out,String style){
        this.is = is;
        this.output = out;
        this.style = style;
    }

    public void run(){
        try {
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line=null;

            while ( (line = br.readLine()) != null){
                if(optionPrintOutput) System.out.println(line);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
