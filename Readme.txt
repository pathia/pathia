Tested and works on a Windows 32bit machine.

#####   COMPILATION EXPLANATION   #####
	To compile run the script "build.bat". Make sure that the path of the project contains no spaces.

#####   EXECUTION EXPLANATION   #####
	First place the two functions you wish to test as two text files called func1.txt and func2.txt in the folder "function". Make sure you name both functions "func".
	Then in order to execute the console app run "execute.bat".

#####   EDITED FILES   #####
	The following files' code was edited from the original JCute source. You can find the added lines in each file by searching for the string "/* Pathia code start */"
	* src/cute/Cute.java
	* src/cute/PathiaData.java (added - did not exist in the original JCute project)
	* src/cute/Call.java
	* src/cute/generateinputandschedule/ArithmeticSolver.java
	* src/cute/generateinputandschedule/GenerateInputAndSchedule.java
	* src/cute/generateinputandschedule/SortedArrayList.java (added - did not exist in the original JCute project)
	* src/cute/concolic/input/InputImpl.java
	* src/cute/concolic/input/InputMap.java
	* src/cute/concolic/pathconstraint/Constraint.java
	* src/cute/comcolic/pathconstraint/DSchedule.java
	* src/cute/comcolic/pathconstraint/PointerConstraint.java
	* src/cute/comcolic/pathconstraint/ScheduleConstraint.java
	* src/cute/comcolic/symbolicexecution/ComputationStack.java
	* src/cute/comcolic/symbolicstate/ArithmeticExpression.java
	* src/cute/gui/CommandLine.java


